## Chile avisa
Te notifica cuando hay nuevas licitaciones en tus rubros en mercado público.
Esta app muestra las próximas licitaciones que hay en tu rubro, sub-rubro.
El sistema procesa diariamente la lista de licitaciones de mercado público, y actualiza el display y notifica a través del app.

sudo apt-get install postgresql

Crear usuario chileavisa en sistema operativo y base de datos mismo nombre, usar password 123456.

sudo adduser chileavisa

sudo su - postgres -c "createuser --createdb --pwprompt chileavisa"  
sudo su - chileavisa -c "createdb chileavisa"  
sudo su - chileavisa -c "createdb chileavisatest"  

git clone https://gitlab.com/hanspoo/chile-avisa

cd chile-avisa  
play deps  
play ec  

..ahora importar proyecto desde eclipse

cd reactjs   
yarn install  
atom . && yarn start  


