import React from "react";
import { connect } from "react-redux";
import Home from "./pages/Home";
import SplashScreen from "./pages/SplashScreen";

const RealApp = ({ pasoActual, comenzar }) => {
  if (pasoActual === "splash") return <SplashScreen comenzar={comenzar} />;
  return <Home />;
};

const s2p = state => {
  return { pasoActual: state.wizard.pasoActual };
};
const d2p = dispatch => {
  return {
    comenzar: () => dispatch({ type: "COMPLETAR_SPLASH" })
  };
};
export default connect(
  s2p,
  d2p
)(RealApp);
