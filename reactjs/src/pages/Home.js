/* eslint react/forbid-prop-types: 0 */
import React from "react";
import {
  AppBar,
  Typography,
  Toolbar,
  withStyles,
  Icon,
  CircularProgress
} from "@material-ui/core";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import RegionesExpandibles from "../components/RegionesExpandibles";
import AreasExpandibles from "../components/AreasExpandibles";
import LicitacionesList from "../components/LicitacionesList";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  },
  input: {
    display: "none"
  }
});

class Home extends React.Component {
  componentWillMount() {
    this.props.cargarFiltros();
    this.props.cargarLicitaciones();
  }

  completarRegion = () => {
    this.props.completarRegiones();
    window.scrollTo(0, 0);
  };

  mainContent = () => {
    const {
      wizard: { pasoActual },
      filtrosReducer: { filtros }
    } = this.props;

    const { areas, regiones } = filtros;

    const { licitaciones } = this.props.licsService;

    if (pasoActual === "regiones")
      return (
        <RegionesExpandibles
          listRegiones={regiones}
          onSubmit={this.completarRegion}
        />
      );

    if (pasoActual === "areas") return <AreasExpandibles areas={areas} />;

    if (pasoActual === "landing") {
      return <LicitacionesList licitaciones={licitaciones} />;
    }
  };
  render() {
    const {
      wizard: { pasoActual },
      filtrosReducer: { loading, error }
    } = this.props;

    if (loading) return <CircularProgress />;
    if (error) return <p>Problemas al leer areas: {error}</p>;

    const { licitaciones } = this.props.licsService;

    if (!licitaciones) return <p>Cargando licitaciones...</p>;

    return (
      <div style={{ paddingBottom: 14 }}>
        <AppBar position="static" color="default">
          <Toolbar>
            {pasoActual === "areas" && (
              <a href="#">
                <Icon onClick={() => this.props.volverEtapa()}>
                  arrow_back_ios
                </Icon>
              </a>
            )}

            <Typography variant="title" color="inherit">
              Chile Avisa
            </Typography>
          </Toolbar>
        </AppBar>
        {this.mainContent()}
      </div>
    );
  }
}

Home.propTypes = {
  classes: PropTypes.object.isRequired
};

const s2p = state => {
  return {
    wizard: state.wizard,
    filtrosReducer: state.filtrosReducer,
    licsService: state.licsService
  };
};

const d2p = dispatch => {
  return {
    completarRegiones: () => dispatch({ type: "COMPLETAR_REGIONES" }),
    volverEtapa: () => dispatch({ type: "VOLVER_ETAPA" }),
    cargarLicitaciones: () =>
      dispatch({ type: "LICITACIONES_FETCH_REQUESTED" }),
    cargarFiltros: () =>
      dispatch({
        type: "FILTROS_FETCH_REQUESTED"
      })
  };
};

const hocRedux = connect(
  s2p,
  d2p
)(Home);

export default withStyles(styles)(hocRedux);
