/* eslint react/forbid-prop-types: 0 */

import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Paper, Button } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  }
});

const margin = 7;

function SpashScreen({ classes, comenzar }) {
  return (
    <div>
      <Paper
        className={classes.root}
        elevation={0}
        style={{
          display: "flex",
          flexDirection: "column",
          backgroundColor: "#775",

          height: "100vh",
          justifyContent: "center",
          alignItems: "flex-start"
        }}
      >
        <Typography
          style={{ margin, color: "white" }}
          variant="headline"
          component="h3"
        >
          Chile avisa
        </Typography>
        <Typography component="p" style={{ margin, color: "white" }}>
          Te notifica cuando hay nuevas licitaciones en tus rubros en mercado
          público.
        </Typography>
        <Button
          style={{ margin }}
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={() => comenzar()}
        >
          Comenzar
        </Button>
      </Paper>
    </div>
  );
}

SpashScreen.propTypes = {
  classes: PropTypes.object.isRequired,
  comenzar: PropTypes.func.isRequired
};

export default withStyles(styles)(SpashScreen);
