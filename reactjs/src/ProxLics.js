import React, { Component } from "react";
import axios from "axios";
import logo from "./logo.svg";
import "./App.css";
import CircularProgress from "@material-ui/core/CircularProgress";
import Licitaciones from "./components/Licitaciones";

const url = "http://localhost:9000/api/licitaciones";
class App extends Component {
	state = {
		lics: null,
		loading: true
	};

	componentWillMount() {
		axios
			.get(url)
			.then(res => {
				this.setState({ lics: res.data, loading: false });
			})
			.catch(e => alert(e));
	}

	render() {
		const { lics, loading } = this.state;
		if (loading) {
			return (
				<div className="centered">
					<CircularProgress />
				</div>
			);
		}
		return <Licitaciones lics={lics} />;
	}
}

export default App;
