import _ from "lodash";

const INITIAL_STATE = {
  mostrarWizard: true,
  pasoActual: "splash",
  regiones: [],
  areas: [],
  subAreas: []
};

const reducer = (state = INITIAL_STATE, action) => {
  console.log("state, action", state, action);
  switch (action.type) {
    case "COMPLETAR_SPLASH": {
      return { ...state, pasoActual: "regiones" };
    }
    case "COMPLETAR_REGIONES": {
      return { ...state, pasoActual: "areas" };
    }
    case "COMPLETAR_AREAS": {
      return { ...state, pasoActual: "landing" };
    }
    case "VOLVER_ETAPA": {
      const nextPaso = state.pasoActual === "areas" ? "regiones" : "landing";
      return { ...state, pasoActual: nextPaso };
    }

    case "TOGGLE_REGION": {
      const { id, checked } = action.payload;
      if (checked) {
        if (_.indexOf(state.regiones, id) !== -1) return state;
        return { ...state, regiones: state.regiones.concat(id) };
      }
      return {
        ...state,
        regiones: state.regiones.filter(iter => iter !== id)
      };
    }

    case "TOGGLE_SUB_AREA": {
      const { id, checked } = action.payload;
      if (checked) {
        console.log(_.indexOf(state.subAreas, id));
        if (_.indexOf(state.subAreas, id) !== -1) return state;
        console.log("concatenando id");
        return { ...state, subAreas: state.subAreas.concat(id) };
      }
      return {
        ...state,
        subAreas: state.subAreas.filter(iter => iter !== id)
      };
    }

    default:
      return state;
  }
};

export default reducer;
