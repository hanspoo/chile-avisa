import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";

const fetchFiltros = () => axios.get("/api/filtros");

function* cargaFiltros() {
  try {
    const result = yield call(fetchFiltros);
    yield put({ type: "FILTROS_FETCH_SUCCEEDED", payload: result.data });
  } catch (e) {
    // // // console.log("tercer yield");
    yield put({ type: "FILTROS_FETCH_FAILED", payload: e.message });
  }
}

function* mySaga() {
  // // // console.log("mySage");
  yield takeLatest("FILTROS_FETCH_REQUESTED", cargaFiltros);
}

export default mySaga;
