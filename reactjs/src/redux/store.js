import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import sagaFiltros from "./sagaFiltros";
import sagaLicitacionesHome from "./sagaLicitacionesHome";
import filtrosReducer from "./filtrosReducer";
import licsService from "./licsService";
import wizard from "./wizard";

const persistConfig = {
  key: "chileavisa",
  storage
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddle = createSagaMiddleware();

const rootReducer = combineReducers({
  wizard,
  filtrosReducer,
  licsService
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(sagaMiddle))
);
sagaMiddle.run(sagaFiltros);
sagaMiddle.run(sagaLicitacionesHome);

const persistor = persistStore(store);

export { store, persistor };
