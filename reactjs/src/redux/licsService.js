const initialState = {
  licitaciones: [],
  loading: true,
  error: false
};
const licsService = (state = initialState, action) => {
  // // // console.log("action.type", action.type);
  switch (action.type) {
    case "LICITACIONES_FETCH_REQUESTED":
      return initialState;
    case "LICITACIONES_FETCH_FAILED":
      return { ...initialState, loading: false, error: action.payload };
    case "LICITACIONES_FETCH_SUCCEEDED":
      return { ...state, loading: false, licitaciones: action.payload };
    default:
      return state;
  }
};

export default licsService;
