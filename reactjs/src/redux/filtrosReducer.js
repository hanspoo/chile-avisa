const initialState = {
  filtros: null,
  loading: true,
  error: false
};
const filtrosReducer = (state = initialState, action) => {
  // // // console.log("action.type", action.type);
  switch (action.type) {
    case "FILTROS_FETCH_REQUESTED":
      return initialState;
    case "FILTROS_FETCH_FAILED":
      return { ...initialState, loading: false, error: action.payload };
    case "FILTROS_FETCH_SUCCEEDED":
      return { ...state, loading: false, filtros: action.payload };
    default:
      return state;
  }
};

export default filtrosReducer;
