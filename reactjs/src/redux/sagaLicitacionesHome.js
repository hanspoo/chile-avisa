import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";

const fetchLicitacionesHome = () => axios.get("/api/licitaciones");

function* cargaLicitaciones() {
  try {
    // // // console.log("primer yield");
    const result = yield call(fetchLicitacionesHome);
    // // // console.log("segundo yield");
    yield put({ type: "LICITACIONES_FETCH_SUCCEEDED", payload: result.data });
  } catch (e) {
    // // // console.log("tercer yield");
    yield put({ type: "LICITACIONES_FETCH_FAILED", payload: e.message });
  }
}

function* mySaga() {
  yield takeLatest("LICITACIONES_FETCH_REQUESTED", cargaLicitaciones);
}

export default mySaga;
