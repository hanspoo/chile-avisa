const normalizar = text => {
  const str = text.trim().toLowerCase();

  return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
};

export { normalizar };
