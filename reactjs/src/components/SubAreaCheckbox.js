import React from "react";
import { FormGroup, FormControlLabel, Checkbox } from "@material-ui/core";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import _ from "lodash";

class SubArea extends React.Component {
  render() {
    const {
      isChecked,
      subArea: { id, nombre, numLics }
    } = this.props;
    return (
      <FormGroup row key={id}>
        <FormControlLabel
          control={
            <Checkbox
              color="default"
              checked={isChecked(id)}
              onClick={e => this.props.toggleSubArea(id, e.target.checked)}
            />
          }
          label={`${nombre} (${numLics}) `}
        />
      </FormGroup>
    );
  }
}

const { object, func } = PropTypes;

SubArea.propTypes = {
  isChecked: func.isRequired,
  subArea: object.isRequired
};

const s2p = state => {
  return { isChecked: id => _.indexOf(state.wizard.subAreas, id) !== -1 };
};
const d2p = dispatch => {
  return {
    toggleSubArea: (id, checked) => {
      console.log("toggle llamado");
      dispatch({ type: "TOGGLE_SUB_AREA", payload: { id, checked } });
    }
  };
};
export default connect(
  s2p,
  d2p
)(SubArea);
