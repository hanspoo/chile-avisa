/* eslint react/forbid-prop-types: 0 */

import React from "react";
import {
  Button,
  ExpansionPanel,
  ExpansionPanelSummary,
  Typography,
  ExpansionPanelDetails,
  withStyles
} from "@material-ui/core";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Region from "./Region";

const { func, object, arrayOf, shape, number } = PropTypes;

const RegionesExpandibles = ({
  regiones,
  classes,
  listRegiones,
  toggleRegion,
  onSubmit
}) => (
  <div>
    <ExpansionPanel expanded>
      <ExpansionPanelSummary
        classes={{ root: "fondo-gris" }}
        expandIcon={<ExpandMoreIcon />}
      >
        <Typography className={classes.heading}>
          Seleccionar región(es)
        </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <div style={{ paddingLeft: "0em" }}>
          {listRegiones.map(ele => (
            <Region
              isChecked={regiones.indexOf(ele.region.id) !== -1}
              toggleRegion={toggleRegion}
              key={ele.region.id}
              {...ele}
            />
          ))}
        </div>
      </ExpansionPanelDetails>
    </ExpansionPanel>
    <Button
      disabled={regiones.length === 0}
      variant="contained"
      color="primary"
      className={classes.button}
      onClick={() => onSubmit()}
    >
      Continuar &raquo;
    </Button>
  </div>
);

const styles = {
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
};

RegionesExpandibles.propTypes = {
  onSubmit: func.isRequired,
  regiones: arrayOf(number).isRequired,
  listRegiones: arrayOf(shape({ region: object, total: number })).isRequired,
  classes: PropTypes.object.isRequired
};

const ContainerRegionesExpandibles = connect(
  state => {
    return { regiones: state.wizard.regiones };
  },
  dispatch => {
    return {
      toggleRegion: (id, checked) =>
        dispatch({ type: "TOGGLE_REGION", payload: { id, checked } })
    };
  }
)(RegionesExpandibles);

export default withStyles(styles)(ContainerRegionesExpandibles);
