/* eslint react/forbid-prop-types: 0 */

import React from "react";
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  Typography,
  ExpansionPanelDetails,
  withStyles,
  Button
} from "@material-ui/core";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import _ from "lodash";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import SearchBar from "./SearchBar";
import { normalizar } from "../utils";
import SubArea from "./SubAreaCheckbox";
import AreasSelected from "./AreasSelected";

const Area = ({ id, nombre, cantidad, subAreas, classes }) => (
  <ExpansionPanel key={id}>
    <ExpansionPanelSummary
      classes={{ root: "fondo-gris" }}
      expandIcon={<ExpandMoreIcon />}
    >
      <Typography className={classes.heading}>
        {nombre} ({cantidad})
      </Typography>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails>
      <div style={{ paddingLeft: "0em" }}>
        {subAreas.map(sa => (
          <SubArea classes={classes} key={sa.id} subArea={sa} />
        ))}
      </div>
    </ExpansionPanelDetails>
  </ExpansionPanel>
);

const ModoAreas = ({ areas, classes }) => {
  return areas.map(area => <Area key={area.id} classes={classes} {...area} />);
};

const ModoSearch = ({ search, areas, classes }) => {
  const subAreas = _.flatten(areas.map(a => a.subAreas));

  const filteredList = subAreas.filter(a => a.search.indexOf(search) !== -1);
  return (
    <div style={{ paddingLeft: "0em" }}>
      {filteredList.map(sa => (
        <SubArea classes={classes} key={sa.id} subArea={sa} />
      ))}
    </div>
  );
};

class AreasExpandibles extends React.Component {
  state = { search: "" };
  onSearch = text => {
    this.setState({ search: normalizar(text) });
  };

  onSubmit = () => {
    this.props.completarAreas();
  };

  render() {
    const { classes, areas, numSelected } = this.props;
    const { search } = this.state;

    return (
      <div>
        <div style={{ marginTop: 14, marginBottom: 14 }}>
          <AreasSelected />
          <SearchBar onChange={this.onSearch} />
          {search ? (
            <ModoSearch classes={classes} search={search} areas={areas} />
          ) : (
            <ModoAreas classes={classes} areas={areas} />
          )}
        </div>
        <Button
          disabled={numSelected === 0}
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={this.onSubmit}
        >
          Continuar &raquo;
        </Button>
      </div>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
};

AreasExpandibles.propTypes = {
  classes: PropTypes.object.isRequired
};

const conEstilos = withStyles(styles)(AreasExpandibles);

const s2p = state => {
  return {
    numSelected: state.wizard.subAreas.length
  };
};

const d2p = dispatch => {
  return {
    completarAreas: () => dispatch({ type: "COMPLETAR_AREAS" })
  };
};

export default connect(
  s2p,
  d2p
)(conEstilos);
