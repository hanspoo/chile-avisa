import React from "react";
import { FormGroup, FormControlLabel, Checkbox } from "@material-ui/core";
import _ from "lodash";

const SubArea = ({ id, selected, nombre, numLics, onChange }) => {
  return (
    <FormGroup row key={id}>
      <FormControlLabel
        control={
          <Checkbox checked={selected} onChange={onChange} color="default" />
        }
        label={`${nombre} (${numLics}) `}
      />
    </FormGroup>
  );
};

const Area = ({ selected, nombre, cantidad, onChange, subAreas }) => {
  return (
    <div>
      <FormControlLabel
        control={
          <Checkbox checked={selected} onChange={onChange} color="default" />
        }
        label={`${nombre} (${cantidad}) `}
      />
      <div style={{ paddingLeft: "2em" }}>
        {subAreas.map(sa => <SubArea {...sa} />)}
      </div>
    </div>
  );
};

export default class Areas extends React.Component {
  state = { selected: [] };

  estaSeleccionada = id => {
    const { selected } = this.state;
    return _.indexOf(selected, id) !== -1;
  };

  areaClicked = clickedId => {
    const { selected } = this.state;

    const estaSele = this.estaSeleccionada(clickedId);
    // // console.log(clickedId, estaSele);

    if (estaSele)
      this.setState({ selected: selected.filter(id => id !== clickedId) });
    else {
      this.setState({ selected: [...selected, clickedId] });
    }
  };

  render() {
    const { areas } = this.props;

    return (
      <div style={styles.container}>
        <div>Hay {areas.length} areas</div>
        {areas.map(a => (
          <FormGroup row key={a.id}>
            <Area
              selected={this.estaSeleccionada(a.id)}
              {...a}
              onChange={() => this.areaClicked(a.id)}
            />
          </FormGroup>
        ))}
      </div>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
};
