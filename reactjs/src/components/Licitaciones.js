import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import ImageIcon from "@material-ui/icons/Image";
import WorkIcon from "@material-ui/icons/Work";
import BeachAccessIcon from "@material-ui/icons/BeachAccess";
import SearchBar from "./SearchBar";

const styles = theme => ({
	root: {
		width: "100%",
		maxWidth: 360,
		backgroundColor: theme.palette.background.paper
	}
});

const LicitacionItem = ({
	numAdquisicion,
	area,
	org,
	subArea,
	nombre,
	cierre: { date: { year, month, day } }
}) => (
	<div>
		<ListItem>
			<Avatar>
				<ImageIcon />
			</Avatar>
			<small
				className="MuiTypography-colorTextSecondary-103"
				style={{ marginRight: 14, position: "absolute", right: "0" }}
			>
				Cierre {`${day}/${month}/${year}`}
			</small>

			<ListItemText
				onClick={() =>
					(window.location.href = "https://www.mercadopublico.cl/Home")
				}
				primary={org}
				secondary={`${nombre} ${numAdquisicion}`}
			/>
		</ListItem>
	</div>
);

function Licitaciones(props) {
	const { lics } = props;
	if (lics.length === 0) {
		return <p>No hay licitaciones</p>;
	}
	return (
		<div>
			<SearchBar />
			<List>{lics.map(lic => <LicitacionItem {...lic} />)}</List>
		</div>
	);
}

Licitaciones.propTypes = {
	lics: PropTypes.array.isRequired
};

export default withStyles(styles)(Licitaciones);
