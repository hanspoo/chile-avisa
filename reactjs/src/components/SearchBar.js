import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const styles = theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

class TextFields extends React.Component {
  onChangeInternal = e => {
    const {
      target: { value }
    } = e;
    // if (e.length < 4) return;
    const func = _.debounce(this.props.onChange, 200);
    func(value);
  };
  render() {
    const { classes, placeholder } = this.props;

    return (
      <form className={classes.container} noValidate>
        <TextField
          onChange={this.onChangeInternal}
          id="search"
          placeholder={placeholder || "Buscar"}
          type="search"
          className={classes.textField}
          margin="normal"
          fullWidth
        />
      </form>
    );
  }
}

const { func } = PropTypes;
TextFields.propTypes = {
  onChange: func.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TextFields);
