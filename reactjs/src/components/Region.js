import React from "react";
import { FormGroup, FormControlLabel, Checkbox } from "@material-ui/core";
import PropTypes from "prop-types";

const { func, bool } = PropTypes;
const Region = ({ isChecked, toggleRegion, region: { id, nombre }, total }) => {
  return (
    <FormGroup row key={id}>
      <FormControlLabel
        control={
          <Checkbox
            color="default"
            checked={isChecked}
            onClick={e => toggleRegion(id, e.target.checked)}
          />
        }
        label={`${nombre} (${total}) `}
      />
    </FormGroup>
  );
};

Region.propTypes = {
  isChecked: bool.isRequired,
  toggleRegion: func.isRequired
};

export default Region;
