import React from "react";
import PropTypes from "prop-types";
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  Typography,
  ExpansionPanelDetails,
  withStyles
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const Licitacion = ({
  id,
  nombre,
  classes,
  org,
  area,
  subArea,
  numAdquisicion
}) => (
  <ExpansionPanel key={id}>
    <ExpansionPanelSummary
      classes={{ root: "fondo-gris" }}
      expandIcon={<ExpandMoreIcon />}
    >
      <Typography className={classes.heading}>
        {nombre} ({numAdquisicion})
      </Typography>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails>
      <Typography variant="body1" gutterBottom>
        <b>Organización</b>: {org}
        <br />
        <b>Area</b>: {area}
        <br />
        <b>Sub Area</b>: {subArea}
        <br />
      </Typography>
    </ExpansionPanelDetails>
  </ExpansionPanel>
);

class LicitacionesList extends React.Component {
  static propTypes = {
    licitaciones: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        nombre: PropTypes.string.isRequired
      })
    ).isRequired
  };
  render() {
    const { licitaciones, classes } = this.props;
    return (
      <div>
        {licitaciones.map(licitacion => (
          <Licitacion classes={classes} {...licitacion} />
        ))}
      </div>
    );
  }
}

const styles = {};
export default withStyles(styles)(LicitacionesList);
