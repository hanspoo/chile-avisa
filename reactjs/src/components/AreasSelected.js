import React from "react";
import { connect } from "react-redux";
import { Chip } from "@material-ui/core";
import _ from "lodash";

const SubArea = ({ id, nombre, removeSubArea }) => (
  <Chip onDelete={() => removeSubArea(id)} label={nombre} />
);

class AreasSelected extends React.Component {
  render() {
    const { subAreas, ids } = this.props;

    const list = subAreas.filter(sa => _.indexOf(ids, sa.id) !== -1);
    return (
      <div style={styles.container}>
        {list.map(sa => (
          <SubArea
            removeSubArea={this.props.removeSubArea}
            key={sa.id}
            {...sa}
          />
        ))}
      </div>
    );
  }
}

const styles = {
  container: {}
};

const s2p = state => {
  return {
    ids: state.wizard.subAreas,
    subAreas: _.flatten(state.filtrosReducer.filtros.areas.map(a => a.subAreas))
  };
};

const d2p = dispatch => {
  return {
    removeSubArea: id =>
      dispatch({ type: "TOGGLE_SUB_AREA", payload: { id, checked: false } })
  };
};

export default connect(
  s2p,
  d2p
)(AreasSelected);
