const INITIAL_STATE = {
  indicadores: null,
  loading: true,
  error: ""
};
const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "INDICADORES_FETCH_REQUESTED":
      return INITIAL_STATE;
    case "INDICADORES_FETCH_SUCCEEDED":
      return { loading: false, indicadores: action.payload };
    case "INDICADORES_FETCH_FAILED":
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export default reducer;
