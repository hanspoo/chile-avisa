/* eslint no-unused-vars: 0 */

import React from "react";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { store, persistor } from "./redux/store";
import RealApp from "./RealApp";

import Home from "./pages/Home";
import SplashScreen from "./pages/SplashScreen";
import css from "./App.css";

export default class App extends React.Component {
  render() {
    // persistor.purge();
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RealApp />
        </PersistGate>
      </Provider>
    );
  }
}
