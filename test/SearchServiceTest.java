import static org.hamcrest.CoreMatchers.is;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import clientes.Cliente;
import clientes.NotificadorClientes;
import clientes.NotificacionCliente;
import models.Area;
import models.Licitacion;
import models.Region;
import models.SubArea;
import search.PublicadasHoy;
import search.SearchService;

public class SearchServiceTest extends TestConRollback {

	@Test
	public void sinLicitacionesNoEncuentraNada() {

		SearchService search = new PublicadasHoy();
		List<Licitacion> lics = search.execute();
		assertThat(lics.size(), is(0));

	}

	@Test
	public void licitacionNuevaDebeNotificar() {

		Licitacion lic = new LicitacionBuilder().build();

		SearchService search = new PublicadasHoy();
		List<Licitacion> lics = search.execute();
		assertThat(lics.size(), is(1));

	}

	@Test
	public void noNotificaCreadasAyerSoloHoy() {

		licitacionesPublicadasAyerHoy();

		SearchService search = new PublicadasHoy();
		List<Licitacion> lics = search.execute();
		assertThat(lics.size(), is(1));

	}

	@Test
	public void todasAntiguas() {

		Licitacion lic1 = new LicitacionBuilder().publicadaAyer().build();
		Licitacion lic2 = new LicitacionBuilder().publicadaAyer().build();

		SearchService search = new PublicadasHoy();
		List<Licitacion> lics = search.execute();
		assertThat(lics.size(), is(0));

	}

	@Test
	public void noHayCientesNoNotificaNadie() {

		licitacionesPublicadasAyerHoy();

		SearchService search = new PublicadasHoy();
		List<Licitacion> lics = search.execute();

		List<Cliente> clientes = Collections.EMPTY_LIST;

		List<NotificacionCliente> notificaciones = new NotificadorClientes(clientes, lics).execute();
		assertThat(notificaciones.size(), is(0));

	}

	@Test
	public void clienteSubAreDefectoSoftwareHaceMatch() {

		SubArea software = SubArea.SOFTWARE();

		Licitacion lic2 = new LicitacionBuilder().build();

		SearchService search = new PublicadasHoy();
		List<Licitacion> lics = search.execute();

		Cliente cli1 = new ClienteBuilder().build();

		List<Cliente> clientes = Cliente.findAll();
		assertThat(clientes.size(), is(1));

		List<NotificacionCliente> notificaciones = new NotificadorClientes(clientes, lics).execute();
		assertThat(notificaciones.size(), is(1));

	}

	@Test
	public void clieneOtraAreaNoNotificar() {

		SubArea legales = SubArea.SERVICIOS_LEGALES();

		Licitacion lic2 = new LicitacionBuilder().build();

		SearchService search = new PublicadasHoy();
		List<Licitacion> lics = search.execute();

		Cliente cli1 = new ClienteBuilder().conSubArea(legales).build();
		List<Cliente> clientes = Cliente.findAll();

		List<NotificacionCliente> notificaciones = new NotificadorClientes(clientes, lics).execute();
		assertThat(notificaciones.size(), is(0));

	}

	private void licitacionesPublicadasAyerHoy() {
		Licitacion lic1 = new LicitacionBuilder().publicadaAyer().build();
		Licitacion lic2 = new LicitacionBuilder().build();
	}

}
