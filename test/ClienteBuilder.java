import org.apache.commons.lang.RandomStringUtils;

import clientes.Cliente;
import models.SubArea;

public class ClienteBuilder {

	SubArea subArea;

	public Cliente build() {

		Cliente c = new Cliente(RandomStringUtils.randomAlphabetic(6));
		c.subAreas.add(this.subArea == null ? SubArea.SOFTWARE() : subArea);
		c.save();
		return c;
	}

	public ClienteBuilder conSubArea(SubArea subArea) {
		this.subArea = subArea;
		return this;
	}

}
