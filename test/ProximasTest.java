import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import clientes.Cliente;
import models.Licitacion;
import models.Region;
import models.SubArea;
import play.Logger;
import search.BuscarPorSubAreaRegion;
import search.SearchService;

public class ProximasTest extends TestConRollback {

	Region metro = Region.METRO();
	SubArea software = SubArea.SOFTWARE();
	SubArea legales = SubArea.SERVICIOS_LEGALES();

	LocalDateTime ayer = LocalDateTime.now().minusDays(1);
	LocalDateTime hoy = LocalDateTime.now();
	LocalDateTime mañana = LocalDateTime.now().plusDays(1);

	Licitacion softAyer = Licitacion.of(software, ayer, ayer);
	Licitacion legalesAyer = Licitacion.of(legales, ayer, ayer);

	Licitacion softHoy = Licitacion.of(software, hoy, hoy);
	Licitacion legalesHoy = Licitacion.of(legales, hoy, hoy);

	Licitacion softMañana = Licitacion.of(software, mañana, mañana);
	Licitacion legalesMañana = Licitacion.of(legales, mañana, mañana);

	Licitacion[] todas = { softAyer, legalesAyer, softHoy, legalesHoy, softMañana, legalesMañana };
	{
		LIC: for (Licitacion l : todas) {
			l.region = metro;
			if (l.ambito == null) {
				Logger.error("Licitación %s ambito nulo", l.numAdquisicion);
				continue LIC;
			}
			l.save();
		}
	}

	Cliente c1 = new Cliente("Software para las masas Ltda.", software);
	Cliente c2 = new Cliente("Abogados Ltda,", legales);

	{
		c1.save();
		c2.save();
	}

	@Ignore
	@Test
	public void proximasSoftware() {

		SearchService service = new BuscarPorSubAreaRegion(software);
		List<Licitacion> lics = service.execute();
		assertThat(lics.size(), is(2));
		assertThat(lics, hasItems(softHoy, softMañana));

	}

	@Ignore
	@Test
	public void proximasLegales() {

		SearchService service = new BuscarPorSubAreaRegion(legales);
		List<Licitacion> lics = service.execute();
		assertThat(lics.size(), is(2));
		assertThat(lics, hasItems(legalesHoy, legalesMañana));

	}

	@Ignore
	@Test
	public void proximasSoftLegales() {

		SearchService service = new BuscarPorSubAreaRegion(software, legales);
		List<Licitacion> lics = service.execute();
		assertThat(lics.size(), is(4));
		assertThat(lics, hasItems(softHoy, softMañana, legalesHoy, legalesMañana));

	}

	@Ignore
	@Test
	public void proximasSinParamsTodas() {

		SearchService service = new BuscarPorSubAreaRegion();
		List<Licitacion> lics = service.execute();
		assertThat(lics.size(), is(4));
		assertThat(lics, hasItems(softHoy, softMañana, legalesHoy, legalesMañana));

	}
}
