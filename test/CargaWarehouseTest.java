
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.TypedQuery;

import org.apache.commons.collections4.ListUtils;
import org.junit.Test;

import loading.Agregador;
import models.Area;
import models.Licitacion;
import models.LineaEntrada;
import models.Org;
import models.RangoMonto;
import models.Region;
import models.SubArea;
import models.TipoLicParser;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.JPAPlugin;
import play.test.UnitTest;

public class CargaWarehouseTest extends UnitTest {

	@Test
	public void cargaTabla() {

		Licitacion.deleteAll();
		Org.deleteAll();
		SubArea.deleteAll();
		Area.deleteAll();
		Region.deleteAll();

		creaConfiguraAreas();
		creaConfiguraOrgs();
		JPA.em().flush();
		creaConfiguraRegiones();
		commitRestartTransaction();
		creaLicitaciones();
	}

	private void creaConfiguraAreas() {

		List<Agregador> list = JPA.em()
				.createQuery("select new loading.Agregador(l.nivel1, l.nivel2) from LineaEntrada l", Agregador.class)
				.getResultList();

		Set<Agregador> hash = new HashSet<>();
		hash.addAll(list);

		Map<String, List<Agregador>> map = hash.stream().collect(Collectors.groupingBy(Agregador::getNivel1));
		List<String> keys = map.keySet().stream().sorted((a, b) -> a.compareTo(b)).collect(Collectors.toList());
		for (String key : keys) {

			System.out.println("procesando " + key);
			Area nivel1 = new Area(key);
			List<Agregador> agregadores = map.get(key).stream().sorted((a1, a2) -> a1.nivel2.compareTo(a2.nivel2))
					.collect(Collectors.toList());

			for (Agregador agregador : agregadores) {
				// System.out.println("\t" + agregador.nivel2);
				SubArea item = nivel1.agregarSegundo(agregador.nivel2);
				Logger.debug("\t%d = %s", item.id, item.nombre);
			}

			nivel1.save();

		}

		hash.stream().forEach(a -> System.out.println(a.nivel1));

	}

	private void creaConfiguraOrgs() {

		List<LineaEntrada> lics = LineaEntrada.findAll();
		Set<String> regiones = lics.stream().filter(l -> l.organismo != null && l.organismo.trim().length() > 0)
				.map(l -> l.organismo).collect(Collectors.toSet());
		for (String s : regiones) {
			Org org = new Org(s);
			org.save();
		}

	}

	private void creaConfiguraRegiones() {

		List<LineaEntrada> lics = LineaEntrada.findAll();
		Set<String> regiones = lics.stream().filter(l -> l.region != null && l.region.trim().length() > 0)
				.map(l -> l.region).collect(Collectors.toSet());
		for (String s : regiones) {
			Region r = new Region(s);
			r.save();
		}

	}

	private void creaLicitaciones() {

		Licitacion.deleteAll();

		TypedQuery<LineaEntrada> query = JPA.em().createQuery("SELECT s FROM LineaEntrada s ORDER BY s.id DESC",
				LineaEntrada.class);
		query.setMaxResults(500);
		List<LineaEntrada> all = query.getResultList();

		List<List<LineaEntrada>> lics = ListUtils.partition(all, 100);

		lics.parallelStream().forEach(l -> procesaLista(l));
		
		JPAPlugin.startTx(false);

	}

	private void commitRestartTransaction() {
		JPA.em().getTransaction().commit();
		JPA.em().getTransaction().begin();
	}

	private void procesaLista(List<LineaEntrada> list) {

		JPAPlugin.startTx(false);

		Map<String, Region> cacheRegion = Region.findAll().stream().map(r -> (Region) r)
				.collect(Collectors.toMap(r -> (String) r.nombre, r -> (Region) r));

		Map<String, Area> cacheArea = Area.findAll().stream().map(r -> (Area) r)
				.collect(Collectors.toMap(r -> (String) r.nombre, r -> (Area) r));
		for (LineaEntrada row : list) {
			procesarFila(row, cacheRegion, cacheArea);
		}

		JPAPlugin.closeTx(false);

	}

	private void procesarFila(LineaEntrada linea, Map<String, Region> cacheRegion, Map<String, Area> cacheArea) {

		if (linea.region == null || linea.region.length() == 0) {
			Logger.error("Linea %s sin region, saltando", linea.numAdquisicion);
			return;
		}

		TipoLicParser t = TipoLicParser.parsear(linea.tipoAdquisicion);

		if (t.ambito == null) {
			Logger.error("Linea %s sin ámbito, saltando", linea.numAdquisicion);
			return;
		}

		Licitacion lic = new Licitacion();
		lic.numAdquisicion = linea.numAdquisicion;
		lic.nombre = linea.nombreAdquisicion;

		lic.region = cacheRegion.get(linea.region);
		lic.area = cacheArea.get(linea.nivel1);
		lic.subArea = lic.area.findSubArea(linea.nivel2);

		lic.fecCierre = linea.fechacierre;
		lic.fecPublicacion = linea.fechapublicación;

		lic.descripcion = linea.descripcion1;

		lic.org = Org.findByNombre(linea.organismo);

		lic.ambito = t.ambito;
		lic.rangoMonto = RangoMonto.buscar(t.min, t.max);
		lic.cantidad = linea.cantidad;

		lic.save();

	}

}
