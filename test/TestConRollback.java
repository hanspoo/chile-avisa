
import javax.persistence.EntityTransaction;

import org.junit.After;

import play.db.jpa.JPA;
import play.test.UnitTest;

public abstract class TestConRollback extends UnitTest {

	@After
	public void tearDown() throws Exception {

		try {
			EntityTransaction tx = JPA.em().getTransaction();
			if (tx.isActive()) {
				tx.rollback();
				JPA.em().getTransaction().begin();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
