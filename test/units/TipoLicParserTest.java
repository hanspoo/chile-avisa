package units;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import models.AmbitoLicitacion;
import models.TipoLicParser;
import play.Logger;

@RunWith(Parameterized.class)
public class TipoLicParserTest {

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {

				{ "Licitación de Servicios", AmbitoLicitacion.SERVICIOS, 0, TipoLicParser.MAX_UTM }, //
				{ "Licitación pública inferior a 100 UTM", AmbitoLicitacion.PUBLICA, 0, 100 }, //
				{ "Licitación Pública igual o superior a 1.000 UTM e inferior a 2.000 UTM (LP)",
						AmbitoLicitacion.PUBLICA, 1000, 2000 }, //
				{ "Licitación Privada Mayor a 5000 UTM", AmbitoLicitacion.PRIVADA, 5000, TipoLicParser.MAX_UTM }, //
				{ "Licitación Pública Mayor a 5000 UTM", AmbitoLicitacion.PUBLICA, 5000, TipoLicParser.MAX_UTM }, //
				{ "Licitación Privada igual o superior a 2000 UTM e inferior a 5000 UTM", AmbitoLicitacion.PRIVADA,
						2000, 5000 }, //
				{ "Licitación Privada igual o superior a 1000 UTM e inferior a 2000 UTM", AmbitoLicitacion.PRIVADA,
						1000, 2000 }, //
				{ "Licitación Pública igual o superior a 2.000 UTM e inferior a 5.000 UTM", AmbitoLicitacion.PUBLICA,
						2000, 5000 }, //
				{ "Licitación Pública igual o superior a 100 UTM e inferior a 1.000 UTM", AmbitoLicitacion.PUBLICA, 100,
						1000 }, //
				{ "Licitación Privada Inferior a 100 UTM", AmbitoLicitacion.PRIVADA, 0, 100 }, //
				// { "Licitación Privada igual o superior a 100 UTM e inferior a 1000 UTM",
				// AmbitoLicitacion.PRIVADA, 100,
				// 1000 }, //

		});
	}

	String text;
	AmbitoLicitacion ambito;
	int min;
	int max;

	public TipoLicParserTest(String testParsear, AmbitoLicitacion tipoLic, int min, int max) {
		super();
		this.text = testParsear;
		this.ambito = tipoLic;
		this.min = min;
		this.max = max;
	}

	@Test
	public void test() {
		TipoLicParser t = TipoLicParser.parsear(text);
		assertThat(t.ambito, is(this.ambito));
		assertThat(t.min, is(this.min));
		assertThat(t.max, is(this.max));

	}
}
