import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.ManyToOne;

import models.AmbitoLicitacion;
import models.Area;
import models.Licitacion;
import models.Org;
import models.RangoMonto;
import models.Region;
import models.SubArea;

public class LicitacionBuilder {

	String numAdquisicion;
	String nombre;
	String descripcion;
	LocalDateTime fecPublicacion;
	LocalDateTime fecCierre;
	Region region;
	Area area;
	SubArea subArea;

	public Licitacion build() {

		Licitacion lic = new Licitacion();

		lic.numAdquisicion = "Hello";
		lic.nombre = "Hello";
		lic.descripcion = "Hello";
		lic.fecPublicacion = fecPublicacion == null ? LocalDateTime.now() : fecPublicacion;
		lic.fecCierre = LocalDateTime.now().plusMonths(1);
		lic.region = Region.METRO();
		lic.area = Area.SERVICIOS();
		lic.subArea = SubArea.SOFTWARE();
		lic.org = Org.UNIVERSIDAD_DE_CHILE();

		lic.ambito = AmbitoLicitacion.PUBLICA;
		lic.cantidad = 1;
		lic.rangoMonto = RangoMonto.comunes().get(0);

		lic.save();

		return lic;
	}

	public LicitacionBuilder publicadaAyer() {
		this.fecPublicacion = LocalDateTime.now().minusDays(1);
		return this;
	}

}
