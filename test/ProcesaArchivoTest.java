
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.junit.Ignore;
import org.junit.Test;

import models.LineaEntrada;
import play.db.jpa.JPA;
import play.db.jpa.JPAPlugin;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.test.UnitTest;

public class ProcesaArchivoTest extends UnitTest {

	private static final String ARCHIVO_ZIP = "test/licitaciones.zip";

	@Ignore
	@Test
	public void descargarArchivoCorrectamente() throws IOException {

		WSRequest req = WS.url("http://www.mercadopublico.cl/Portal/att.ashx?id=5");
		HttpResponse res = req.get();
		assertThat(res.getStatus(), is(200));
		InputStream is = res.getStream();

		File file = new File("/tmp/" + RandomStringUtils.randomAlphabetic(6));
		Files.copy(is, file.toPath());

		assertTrue(file.length() > 0);

	}

	@Ignore
	@Test
	public void descomprimeCorrectamenteDosArchivos() throws IOException {

		String[] names = { "Licitacion_Publicada.csv", "Licitacion_Publicada.csv" };
		List<String> expectedFiles = Arrays.asList(names);

		Path targetDir = Files.createTempDirectory("lics");

		loading.Utils.descomprime(ARCHIVO_ZIP, targetDir);

		List<String> archivosDescompridos = fileList(targetDir.toAbsolutePath().toString());

		for (String fileEsperado : expectedFiles) {
			assertTrue(archivosDescompridos.contains(fileEsperado));
		}

		FileUtils.deleteDirectory(new File(targetDir.toAbsolutePath().toString()));

	}

	final String[] headers = { //
			"Numero Adquisición", //
			"Tipo Adquisición", //
			"Nombre Adquisición", //
			"Descripción ", //
			"Organismo ", //
			"Región", //
			"Fecha Publicación", //
			"Fecha Cierre", //
			"Descripción del producto o servicio", //
			"Código ONU", //
			"Unidad de Medida", //
			"Cantidad ", //
			"Genérico ", //
			"Nivel 1", //
			"Nivel 2", //
			"Nivel 3", //
	};

	@Test
	public void extraeUnaFila() throws IOException {

		POIFSFileSystem fs = new POIFSFileSystem(new File("test/una-fila.xls"));
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		HSSFSheet sheet = wb.getSheetAt(0);

		HSSFRow row = sheet.getRow(0);
		HSSFCell cell = row.getCell(1);
		assertThat(cell.getRichStringCellValue().toString(), is("517903-20-L118"));

		LineaEntrada lic = new LineaEntrada(row);

		assertThat(lic.numAdquisicion, is("517903-20-L118"));
		assertThat(lic.tipoAdquisicion, is("Licitación pública inferior a 100 UTM"));
		assertThat(lic.nombreAdquisicion, is("Adq. de vanitorios y urinarios para el CCA."));
		assertThat(lic.descripcion1, is(
				"Adquisición de vanitorios y urinarios  para la remodelación de los baños del Comando Conjunto Austral."));
		assertThat(lic.organismo, is("Estado Mayor Conjunto"));
		assertThat(lic.region, is("Región de Magallanes y de la Antártica"));
		assertThat(lic.fechapublicación, is(LocalDateTime.of(LocalDate.of(2018, 7, 12), LocalTime.of(7, 3, 22))));
		assertThat(lic.fechacierre, is(LocalDateTime.of(LocalDate.of(2018, 7, 19), LocalTime.of(11, 0, 0))));
		assertThat(lic.descripcion2, is(
				"Adquisición de vanitorios y urinarios para la remodelación de los baños del Comando Conjunto Austral."));
		assertThat(lic.codigoOnu, is(72102801));
		assertThat(lic.unidadMedida, is("Unidad"));
		assertThat(lic.cantidad, is(1));
		assertThat(lic.generico, is("Renovación de edificios, casas, señales y monumentos"));

		assertThat(lic.nivel1, is("Servicios de construcción y mantenimiento"));
		assertThat(lic.nivel2, is("Servicios de atención, mantenimiento y reparaciones de edificios"));
		assertThat(lic.nivel3, is("Servicios de restauración de edificios y estructuras"));

	}

	@Ignore
	@Test
	public void validaFormatoDelCSV() throws IOException {

		LineaEntrada.deleteAll();
		commitRestartTransaction();

		Path targetDir = Files.createTempDirectory("lics");
		loading.Utils.descomprime(ARCHIVO_ZIP, targetDir);

		Path file = extraeArchivoCumpleCon(targetDir, (Path p) -> p.getFileName().toString().endsWith(".xls"));
		assertThat(file.getFileName().toString(), is("Licitacion_Publicada.xls"));

		POIFSFileSystem fs = new POIFSFileSystem(Files.newInputStream(file));
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		HSSFSheet sheet = wb.getSheetAt(0);

		List<HSSFRow> filas = new ArrayList<>();
		sheet.rowIterator().forEachRemaining(r -> filas.add((HSSFRow) r));

		List<List<HSSFRow>> particiones = ListUtils.partition(filas, 500);
		particiones.parallelStream().forEach(l -> procesaLista(l));

		// int nRows = sheet.getPhysicalNumberOfRows();
		//
		// LINEA: for (int i = 0; i < nRows; i++) {
		// System.out.println(i);
		// HSSFRow row = sheet.getRow(i);
		//
		// filas.add(row);
		//
		//// HSSFCell cell = row.getCell(1);
		//// if (cell == null)
		//// continue LINEA;
		////
		//// String s = cell.getRichStringCellValue().toString();
		//// if (s == null || s.trim().length() == 0 || !s.matches("\\w*-\\w*-\\w*"))
		//// continue LINEA;
		////
		//// models.LineaEntrada lic = new models.LineaEntrada(row);
		//// lic.save();
		//
		// }

		FileUtils.deleteDirectory(new File(targetDir.toAbsolutePath().toString()));

	}

	private void commitRestartTransaction() {
		JPA.em().getTransaction().commit();
		JPA.em().getTransaction().begin();
	}

	private void procesaLista(List<HSSFRow> list) {

		JPAPlugin.startTx(false);
		for (HSSFRow row : list) {
			procesarFila(row);
		}

		JPAPlugin.closeTx(false);

	}

	private void procesarFila(HSSFRow row) {

		HSSFCell cell = row.getCell(1);
		if (cell == null)
			return;

		String s = cell.getRichStringCellValue().toString();
		if (s == null || s.trim().length() == 0 || !s.matches("\\w*-\\w*-\\w*"))
			return;

		models.LineaEntrada lic = new models.LineaEntrada(row);
		lic.save();

	}

	private Path extraeArchivoCumpleCon(Path directory, Function<Path, Boolean> condicionArchivos) throws IOException {

		Path resultado = null;

		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(directory)) {
			for (Path path : directoryStream) {
				System.out.println("Revisando " + path.getFileName());
				if (condicionArchivos.apply(path)) {
					resultado = path;
				}
			}
		}

		return resultado;

	}


	private static List<String> fileList(String directory) {
		List<String> fileNames = new ArrayList<>();
		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(directory))) {
			for (Path path : directoryStream) {
				fileNames.add(path.getFileName().toString());
			}
		} catch (IOException ex) {
		}
		return fileNames;
	}

}
