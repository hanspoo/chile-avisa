package search;

import java.util.Collections;
import java.util.List;

import models.Licitacion;

public abstract class SearchService {

	public abstract List<Licitacion> execute();
}
