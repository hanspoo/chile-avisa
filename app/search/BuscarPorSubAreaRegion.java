package search;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang.Validate;

import models.AmbitoLicitacion;
import models.Licitacion;
import models.RangoMonto;
import models.Region;
import models.SubArea;

public class BuscarPorSubAreaRegion extends SearchService {

	List<SubArea> subAreas = new ArrayList<>();
	List<Region> regiones = new ArrayList<>();

	public BuscarPorSubAreaRegion(SubArea... subAreas) {
		super();

		for (SubArea subArea : subAreas)
			this.subAreas.add(subArea);

	}

	public BuscarPorSubAreaRegion(SubArea software) {
		super();
		Validate.notNull(software);
		this.subAreas.add(software);
	}

	List<RangoMonto> rangosBasicos = RangoMonto.comunes();

	LocalDate hoy = LocalDate.now();
	LocalDate ayer = LocalDate.now().minusDays(1);
	LocalDate hasta = LocalDate.now().plusMonths(2);

	Predicate<? super Licitacion> fechaFilter = l -> l.diaCierre != null && l.diaCierre.isAfter(ayer)
			&& l.diaCierre.isBefore(hasta) && l.diaPublicacion.getYear() >= hoy.getYear();

	Predicate<Licitacion> filtroComun = (l) -> l.cantidad == 1 && l.ambito == AmbitoLicitacion.PUBLICA
			&& l.subArea == SubArea.SOFTWARE() && l.region == Region.METRO();

	@Override
	public List<Licitacion> execute() {

		List<Licitacion> ordenadas = Licitacion.ordenadas();

		List<Licitacion> res;
		if (subAreas.size() == 0) {
			res = ordenadas.stream().filter(filtroComun).filter(fechaFilter).collect(Collectors.toList());
		} else {
			res = ordenadas.stream().filter(l -> this.subAreas.contains(l.subArea)).filter(fechaFilter)
					.filter(filtroComun).collect(Collectors.toList());
		}

		return res;

	}

}
