package search;

import java.time.LocalDate;
import java.util.List;

import models.Licitacion;

public class PublicadasHoy extends SearchService {

	@Override
	public List<Licitacion> execute() {

		return Licitacion.find("byDiaPublicacion", LocalDate.now()).fetch();
	}

}
