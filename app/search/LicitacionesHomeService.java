package search;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.TypedQuery;

import models.Licitacion;
import play.db.jpa.JPA;

public class LicitacionesHomeService extends SearchService {

	@Override
	public List<Licitacion> execute() {

		TypedQuery<Licitacion> q = JPA.em().createQuery(
				"select l from Licitacion l where lower(nombre) not like '%prueba%' and fecCierre > :ahora order by fecCierre",
				Licitacion.class);

		return q.setParameter("ahora", LocalDateTime.now()).setMaxResults(100).getResultList();

	}

}
