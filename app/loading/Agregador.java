package loading;

public class Agregador {

	public String nivel1;
	public String nivel2;

	public Agregador(String nivel1, String nivel2) {
		super();
		this.nivel1 = nivel1;
		this.nivel2 = nivel2;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nivel1 == null) ? 0 : nivel1.hashCode());
		result = prime * result + ((nivel2 == null) ? 0 : nivel2.hashCode());
		return result;
	}

	public String getNivel1() {
		return nivel1;
	}

	public String getNivel2() {
		return nivel2;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Agregador other = (Agregador) obj;
		if (nivel1 == null) {
			if (other.nivel1 != null)
				return false;
		} else if (!nivel1.equals(other.nivel1))
			return false;
		if (nivel2 == null) {
			if (other.nivel2 != null)
				return false;
		} else if (!nivel2.equals(other.nivel2))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Agregador [nivel1=" + nivel1 + ", nivel2=" + nivel2 + "]";
	}

}