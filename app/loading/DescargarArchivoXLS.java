package loading;

import static org.hamcrest.CoreMatchers.is;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.lang.RandomStringUtils;

import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;

public class DescargarArchivoXLS {

	public File execute() {

		try {

			WSRequest req = WS.url("http://www.mercadopublico.cl/Portal/att.ashx?id=5");
			HttpResponse res = req.get();
			InputStream is = res.getStream();

			File archivoComprimido = new File("/tmp/" + RandomStringUtils.randomAlphabetic(6));
			Files.copy(is, archivoComprimido.toPath());

			Path directorioDescompresion = Files.createTempDirectory("lics");
			loading.Utils.descomprime(archivoComprimido.getAbsolutePath(), directorioDescompresion);

			return new File(directorioDescompresion + File.separator + "Licitacion_Publicada.xls");
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

	}

	
}
