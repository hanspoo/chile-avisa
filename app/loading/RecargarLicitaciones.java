package loading;

import java.io.File;

import javax.persistence.EntityManager;

import etl.EtlFactLicitacion;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.JPAPlugin;

public class RecargarLicitaciones {

	public void execute() {

		DescargarArchivoXLS des = new DescargarArchivoXLS();
		File file = des.execute();
		Logger.debug("Archivo %s descargado", file.getAbsolutePath());

		CargarLineasEntradaDesdeArchivo c = new CargarLineasEntradaDesdeArchivo(file);
		c.execute();
		Logger.debug("Procesadas las líneas de entrada");

		EntityManager localEm = JPA.newEntityManager("default");
		JPA.bindForCurrentThread("default", localEm, false);
		JPA.em().getTransaction().begin();

		EtlFactLicitacion etl = new EtlFactLicitacion();
		etl.execute();
		Logger.debug("ETL ejecutado");

	}

}