package loading;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import models.LineaEntrada;
import play.Logger;
import play.db.jpa.JPA;
import play.db.jpa.JPAPlugin;

public class CargarLineasEntradaDesdeArchivo {

	File planillaExcel;

	public CargarLineasEntradaDesdeArchivo(File planillaExcel) {
		super();
		this.planillaExcel = planillaExcel;
	}

	public void execute() {

		LineaEntrada.deleteAll();
		commitRestartTransaction();

		POIFSFileSystem fs;
		try {
			fs = new POIFSFileSystem(Files.newInputStream(this.planillaExcel.toPath()));

			try (HSSFWorkbook wb = new HSSFWorkbook(fs)) {
				HSSFSheet sheet = wb.getSheetAt(0);

				List<HSSFRow> filas = new ArrayList<>();
				sheet.rowIterator().forEachRemaining(r -> filas.add((HSSFRow) r));

				List<List<HSSFRow>> particiones = ListUtils.partition(filas, 500);
				particiones.parallelStream().forEach(l -> procesaLista(l));
			}

		} catch (IOException e) {

			throw new RuntimeException(e.getMessage());
		}

	}

	private void commitRestartTransaction() {
		try {
			JPA.em().getTransaction().commit();
			JPA.em().getTransaction().begin();

		} catch (Exception e) {
			Logger.debug(e.getMessage());

		}
	}

	@SuppressWarnings("deprecation")
	private void procesaLista(List<HSSFRow> list) {

		JPAPlugin.startTx(false);
		for (HSSFRow row : list) {
			procesarFila(row);
		}

		JPAPlugin.closeTx(false);

	}

	private void procesarFila(HSSFRow row) {

		HSSFCell cell = row.getCell(1);
		if (cell == null)
			return;

		String s = cell.getRichStringCellValue().toString();
		if (s == null || s.trim().length() == 0 || !s.matches("\\w*-\\w*-\\w*"))
			return;

		models.LineaEntrada lic = new models.LineaEntrada(row);
		lic.save();

	}

}
