package utils.jpa;

import models.Region;

public class TotalRegion {

	public Region region;
	public Long total;

	public TotalRegion(Region region, Long total) {
		super();
		this.region = region;
		this.total = total;
	}

	public Region getRegion() {
		return region;
	}

	public Long getTotal() {
		return total;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

}
