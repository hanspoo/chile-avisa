package utils.jpa;

import models.SubArea;
import utils.StringUtils;

public class TotalesSubArea {

	public Integer id;
	public String nombre;
	String search;
	public Long numLics;

	public TotalesSubArea(Integer id, String nombre, Long numLics) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.numLics = numLics;
		this.search = StringUtils.removeDiacritics(nombre);
	}

}
