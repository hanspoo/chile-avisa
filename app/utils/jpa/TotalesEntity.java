package utils.jpa;

import models.Area;

public class TotalesEntity {

	public Area area;
	public Long total;

	public TotalesEntity(Area area, Long total) {
		super();
		this.area = area;
		this.total = total;
	}

	public Area getArea() {
		return area;
	}

	public Long getTotal() {
		return total;
	}
}
