package utils;

import java.text.Normalizer;

public class StringUtils {

	public static String removeDiacritics(String s) 
	{
	    s = Normalizer.normalize(s.trim().toLowerCase(), Normalizer.Form.NFD);
	    s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
	    return s;
	}
}
