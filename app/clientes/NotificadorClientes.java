package clientes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import models.Licitacion;

public class NotificadorClientes {

	List<Cliente> clientes;
	List<Licitacion> lics;

	public NotificadorClientes(List<Cliente> clientes, List<Licitacion> lics) {
		super();
		this.clientes = clientes;
		this.lics = lics;
	}

	public List<NotificacionCliente> execute() {

		if (clientes.size() == 0)
			return Collections.EMPTY_LIST;

		List<NotificacionCliente> list = new ArrayList<>();
		for (Cliente cliente : clientes) {
			List<Licitacion> delCliente = lics.stream().filter(l -> cliente.subAreas.contains(l.subArea))
					.collect(Collectors.toList());
			if (delCliente.size() > 0) {
				NotificacionCliente notifCli = new NotificacionCliente(cliente, delCliente);
				list.add(notifCli);
			}
		}

		return list;

	}

}
