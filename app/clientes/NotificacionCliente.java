package clientes;

import java.util.List;

import models.Licitacion;

public class NotificacionCliente {

	Cliente cliente;
	List<Licitacion> delCliente;

	public NotificacionCliente(Cliente cliente, List<Licitacion> delCliente) {
		super();
		this.cliente = cliente;
		this.delCliente = delCliente;
	}

}
