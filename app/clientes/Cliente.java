package clientes;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.apache.commons.lang.Validate;

import models.SubArea;
import play.db.jpa.Model;

@Entity
public class Cliente extends Model {

	@Column(nullable = false)
	String nombre;

	@OneToMany
	public List<SubArea> subAreas = new ArrayList<>();

	public Cliente(String nombre) {
		super();
		this.nombre = nombre;
	}

	public Cliente(String nombre, SubArea subArea) {
		super();
		Validate.notNull(nombre);
		Validate.notEmpty(nombre);
		Validate.notNull(subArea);

		this.nombre = nombre;
		this.subAreas.add(subArea);
	}

}
