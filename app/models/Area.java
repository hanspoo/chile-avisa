package models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import models.dto.AreaDTO;
import play.db.jpa.GenericModel;
import play.db.jpa.JPA;
import play.db.jpa.JPABase;
import utils.jpa.TotalesEntity;
import utils.jpa.TotalesSubArea;

@Entity

@Table(indexes = {
		@Index(name = "idx_nombre_area", columnList = "nombre") }, uniqueConstraints = @UniqueConstraint(columnNames = "nombre"))
public class Area extends GenericModel {

	@Id
	public int id;

	public Area(String nombre) {
		super();
		this.nombre = nombre;
	}

	@Column(nullable = false)
	public String nombre;

	@OneToMany(mappedBy = "area", cascade = CascadeType.ALL)
	public List<SubArea> subAreas = new ArrayList<>();

	@PrePersist
	public void darId() {
		id = nombre.hashCode();
	}

	public SubArea agregarSegundo(String s) {

		SubArea item = new SubArea(s);

		item.area = this;
		item.id = s.hashCode() + this.nombre.hashCode();

		this.subAreas.add(item);

		return item;

	}

	public SubArea findSubArea(String s) {

		Optional<SubArea> opt = subAreas.stream().filter(sa -> sa.nombre.equalsIgnoreCase(s)).findAny();
		if (opt.isPresent())
			return opt.get();
		else
			throw new RuntimeException(String.format("Sub área %s no encontrada en area %s", s, this.nombre));

	}

	public static Area findByNombre(String s) {
		Area first = Area.find("byNombre", s).first();
		if (first == null)
			throw new RuntimeException("Area " + s + " no encontrada");

		return first;
	}

	public static Area SERVICIOS() {
		return Area.findById(-1452330030);
	}

	public static Collection<Area> ordenadas() {

		Collection<Area> todas = Area.find(" order by nombre ").fetch();
		return todas;

	}

	public AreaDTO dto(Long numLics, List<TotalesSubArea> subAreas) {
		AreaDTO dto = new AreaDTO(this);
		dto.cantidad = numLics == null ? 0L : numLics;

		List<Integer> misIds = this.subAreas.stream().map(sa -> sa.id).collect(Collectors.toList());

		dto.subAreas = subAreas.stream().filter(sa -> misIds.contains(sa.id)).collect(Collectors.toList());

		return dto;
	}

	public static Map<Area, Long> totalesPorArea() {
		String sql = "select new utils.jpa.TotalesEntity(area, count(*)) from Licitacion l group by l.area";
		List<TotalesEntity> list = JPA.em().createQuery(sql, utils.jpa.TotalesEntity.class).getResultList();
		Map<Area, Long> map = list.stream().collect(Collectors.toMap(t -> t.getArea(), t -> t.getTotal()));
		return map;
	}

}
