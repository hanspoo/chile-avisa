package models;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import play.db.jpa.GenericModel;

@Entity
@Table(indexes = {
		@Index(name = "idx_nombre_org", columnList = "nombre") }, uniqueConstraints = @UniqueConstraint(columnNames = "nombre"))
public class Org extends GenericModel {

	@Id
	int id;

	public Org(String nombre) {
		super();
		this.nombre = nombre;
	}

	@Column(nullable = false)
	String nombre;

	@PrePersist
	public void darId() {
		id = nombre.hashCode();
	}

	public static Org findByNombre(String s) {
		return Org.find("byNombre", s).first();
	}

	public static Org UNIVERSIDAD_DE_CHILE() {
		return Org.findById(1900461568);
	}

}
