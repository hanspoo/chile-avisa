package models;

import java.time.LocalDateTime;

public class LicitacionDTO implements Comparable<LicitacionDTO> {

	public String nombre;
	public LocalDateTime cierre;
	public String org;
	public String area;
	public String subArea;
	public String numAdquisicion;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((cierre == null) ? 0 : cierre.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((org == null) ? 0 : org.hashCode());
		result = prime * result + ((subArea == null) ? 0 : subArea.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LicitacionDTO other = (LicitacionDTO) obj;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.equals(other.area))
			return false;
		if (cierre == null) {
			if (other.cierre != null)
				return false;
		} else if (!cierre.equals(other.cierre))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (org == null) {
			if (other.org != null)
				return false;
		} else if (!org.equals(other.org))
			return false;
		if (subArea == null) {
			if (other.subArea != null)
				return false;
		} else if (!subArea.equals(other.subArea))
			return false;
		return true;
	}

	@Override
	public int compareTo(LicitacionDTO o) {

		return this.cierre.compareTo(o.cierre);
	}

}
