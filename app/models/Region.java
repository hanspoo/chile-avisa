package models;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import play.db.jpa.GenericModel;
import play.db.jpa.JPA;
import utils.jpa.TotalRegion;

@Entity
@Table(indexes = {
		@Index(name = "idx_nombre_region", columnList = "nombre") }, uniqueConstraints = @UniqueConstraint(columnNames = "nombre"))
public class Region extends GenericModel {

	@Id
	int id;

	public Region(String nombre) {
		super();
		this.nombre = nombre;
	}

	@Column(nullable = false)
	public String nombre;

	@PrePersist
	public void darId() {
		id = nombre.hashCode();
	}

	public static Region findByNombre(String s) {
		return Region.find("byNombre", s).first();
	}

	public static Region METRO() {

		return Region.findById(1864158552);
	}

	public static List<TotalRegion> totalesPorRegion() {
		String sql = "select new utils.jpa.TotalRegion(region, count(*)) from Licitacion l group by l.region";
		return JPA.em().createQuery(sql, utils.jpa.TotalRegion.class).getResultList();
	}

}
