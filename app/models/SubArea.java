package models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.db.jpa.GenericModel;
import play.db.jpa.JPA;
import utils.StringUtils;
import utils.jpa.TotalesSubArea;

@Entity
public class SubArea extends GenericModel {

	@Id
	public int id;

	@ManyToOne(optional = false)
	public Area area;

	@Column(nullable = false)
	public String nombre;

	String search;

	public SubArea(String nombre) {
		super();
		this.nombre = nombre;
		search = StringUtils.removeDiacritics(nombre);
	}

	public static SubArea SOFTWARE() {
		return SubArea.findById(-68355687);
	}

	public static SubArea SERVICIOS_LEGALES() {
		return SubArea.findById(234460046);
	}

	public static List<TotalesSubArea> totalesPorSubArea() {
		String sql = "select new utils.jpa.TotalesSubArea(l.subArea.id, l.subArea.nombre, count(*)) "
				+ " from Licitacion l group by l.subArea.id, l.subArea.nombre  " + " order by l.subArea.nombre";
		List<TotalesSubArea> list = JPA.em().createQuery(sql, utils.jpa.TotalesSubArea.class).getResultList();
		return list;

	}

}
