package models;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;

import play.Logger;
import play.db.jpa.Model;

@Entity
public class LineaEntrada extends Model {

	@Column(nullable = false)
	public String numAdquisicion;

	@Column(nullable = false)
	public String tipoAdquisicion;

	@Column(nullable = false)
	public String nombreAdquisicion;

	@Column(length = 4096, nullable = false)
	public String descripcion1;

	@Column(nullable = false)
	public String organismo;

	@Column(nullable = false)
	public String region;

	@Column(nullable = false)
	public LocalDateTime fechapublicación;
	public LocalDateTime fechacierre;

	@Column(length = 4096, nullable = false)
	public String descripcion2;

	@Column(nullable = false)
	public Integer codigoOnu;
	@Column(nullable = false)
	public String unidadMedida;
	@Column(nullable = false)
	public Integer cantidad;
	@Column(nullable = false)
	public String generico;
	@Column(nullable = false)
	public String nivel1;
	@Column(nullable = false)
	public String nivel2;
	@Column(nullable = false)
	public String nivel3;

	public LineaEntrada(HSSFRow row) {
		super();

		numAdquisicion = row.getCell(1).getRichStringCellValue().toString();
		tipoAdquisicion = row.getCell(2).getRichStringCellValue().toString();

		nombreAdquisicion = row.getCell(4).getRichStringCellValue().toString();
		descripcion1 = row.getCell(5).getRichStringCellValue().toString();

		organismo = row.getCell(6).getRichStringCellValue().toString();

		region = row.getCell(7).getRichStringCellValue().toString();

		fechapublicación = celdaToLocalDate(row.getCell(9));
		fechacierre = celdaToLocalDate(row.getCell(10));

		descripcion2 = row.getCell(11).getRichStringCellValue().toString();
		codigoOnu = (int) row.getCell(12).getNumericCellValue();
		unidadMedida = row.getCell(13).getRichStringCellValue().toString();
		cantidad = (int) row.getCell(14).getNumericCellValue();
		generico = row.getCell(15).getRichStringCellValue().toString();

		nivel1 = row.getCell(16).getRichStringCellValue().toString();
		nivel2 = row.getCell(17).getRichStringCellValue().toString();
		nivel3 = row.getCell(18).getRichStringCellValue().toString();

	}

	@SuppressWarnings("deprecation")
	private LocalDateTime celdaToLocalDate(HSSFCell cell) {

		int cellType = cell.getCellType();
		if (cellType != 0) {
			return null;
		}

		try {
			Date fecha = cell.getDateCellValue();

			LocalDateTime dateTime = toLocalDateTime(fecha).truncatedTo(ChronoUnit.SECONDS);
			return dateTime;

		} catch (Exception e) {

			Logger.error("Celda tipo %d error %s", cellType, e.getMessage());
			return null;
		}

	}

	@Override
	public String toString() {
		return "LineaEntrada [numAdquisicion=" + numAdquisicion + ", tipoAdquisicion=" + tipoAdquisicion + "]";
	}

	LocalDateTime toLocalDateTime(Date dateToConvert) {
		return new java.sql.Timestamp(dateToConvert.getTime()).toLocalDateTime();
	}

}