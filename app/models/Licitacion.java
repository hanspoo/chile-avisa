package models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.Validate;

import play.db.jpa.Model;

@Entity
public class Licitacion extends Model {

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	public AmbitoLicitacion ambito;

	@ManyToOne(optional = false)
	public RangoMonto rangoMonto;

	@Column(nullable = false)
	public Integer cantidad;

	@Column(nullable = false)
	public String numAdquisicion;

	@Column(nullable = false)
	public String nombre;

	@Column(length = 4096, nullable = false)
	public String descripcion;

	@Column(nullable = false)
	public LocalDateTime fecPublicacion;
	public LocalDateTime fecCierre;

	public LocalDate diaPublicacion;
	public LocalDate diaCierre;

	@ManyToOne(optional = false)
	public Region region;

	@ManyToOne(optional = false)
	public Area area;

	@ManyToOne(optional = false)
	public SubArea subArea;

	@ManyToOne(optional = false)
	public Org org;

	@PrePersist
	public void antesDe() {

		this.nombre = this.nombre.trim();
		this.descripcion = this.descripcion.trim();
		this.numAdquisicion = this.numAdquisicion.trim().toUpperCase();

		this.diaPublicacion = this.fecPublicacion.toLocalDate();
		this.diaCierre = this.fecCierre == null ? null : this.fecCierre.toLocalDate();
	}

	public static List<Licitacion> ordenadas() {

		return Licitacion.find("order by fecCierre").fetch();
	}

	public static Licitacion of(SubArea subArea, LocalDateTime publicacion, LocalDateTime cierre) {

		Validate.notNull(subArea);
		Validate.notNull(publicacion);
		Validate.notNull(cierre);

		Licitacion lic = new Licitacion();

		lic.nombre = RandomStringUtils.randomAlphabetic(6);
		lic.descripcion = lic.nombre;
		lic.numAdquisicion = lic.nombre;

		lic.area = subArea.area;
		lic.org = Org.UNIVERSIDAD_DE_CHILE();

		lic.subArea = subArea;
		lic.fecPublicacion = publicacion;
		lic.fecCierre = cierre;
		
		lic.ambito = AmbitoLicitacion.PUBLICA;
		lic.cantidad = 1;
		lic.rangoMonto = RangoMonto.comunes().get(0);

		return lic;

	}

	public LicitacionDTO dto() {

		LicitacionDTO dto = new LicitacionDTO();
		
		dto.nombre = this.nombre;
		dto.cierre = this.fecCierre;
		dto.org = this.org.nombre;
		dto.area = this.area.nombre;
		dto.subArea = this.subArea.nombre;
		dto.numAdquisicion = this.numAdquisicion;

		return dto;

	}

}
