package models;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Entity;

import play.db.jpa.JPABase;
import play.db.jpa.Model;

@Entity
public class RangoMonto extends Model {

	Integer min;
	Integer max;

	public static RangoMonto buscar(Integer min, Integer max) {

		if (min + max == 0)
			max = TipoLicParser.MAX_UTM;

		List<RangoMonto> all = RangoMonto.findAll();

		for (RangoMonto r : all) {
			if (r.min.equals(min) && r.max.equals(max))
				return r;

		}

		throw new RuntimeException(String.format("Rango %d a %d no encontrado", min, max));

	}

	public static List<RangoMonto> comunes() {
		List<RangoMonto> list = RangoMonto.findAll();

		return list.stream().filter(r -> r.id >= 1 && r.id <= 3).collect(Collectors.toList());
	}

}
