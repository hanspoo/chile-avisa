package models.dto;

import java.util.List;

import models.Area;
import utils.StringUtils;
import utils.jpa.TotalesSubArea;

public class AreaDTO {

	private String nombre;
	private int id;
	public long cantidad;
	public List<TotalesSubArea> subAreas;

	String search;

	public AreaDTO(Area area) {

		this.nombre = area.nombre;
		this.id = area.id;
		search = StringUtils.removeDiacritics(nombre);
	}

}
