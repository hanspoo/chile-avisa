package models;

import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;

import play.db.jpa.Model;

public class TipoLicParser {

	public static final int MAX_UTM = 100000;

	public AmbitoLicitacion ambito;
	public int min;
	public int max;

	public static TipoLicParser parsear(String text) {
		TipoLicParser t = new TipoLicParser();

		BLOCK: {
			Pattern pat1 = Pattern.compile("Licitación (\\S+) igual o superior a (\\S+) UTM e inferior a (\\S+) UTM",
					Pattern.CASE_INSENSITIVE);
			Matcher mat = pat1.matcher(text);
			if (mat.find()) {
				t.ambito = AmbitoLicitacion.valueOf(deAccent(mat.group(1)).toUpperCase());
				t.min = dameInteger(mat.group(2));
				t.max = dameInteger(mat.group(3));
				break BLOCK;
			}

			Pattern pat2 = Pattern.compile("Licitación (\\S+) inferior a (\\S+) UTM", Pattern.CASE_INSENSITIVE);
			mat = pat2.matcher(text);
			if (mat.find()) {
				t.ambito = AmbitoLicitacion.valueOf(deAccent(mat.group(1)).toUpperCase());
				t.min = 0;
				t.max = dameInteger(mat.group(2));
				break BLOCK;
			}

			Pattern pat3 = Pattern.compile("Licitación (\\S+) Mayor a (\\S+) UTM", Pattern.CASE_INSENSITIVE);
			mat = pat3.matcher(text);
			if (mat.find()) {
				t.ambito = AmbitoLicitacion.valueOf(deAccent(mat.group(1)).toUpperCase());
				t.min = dameInteger(mat.group(2));
				t.max = MAX_UTM;
				break BLOCK;
			}

			if (text.equalsIgnoreCase("Licitación de Servicios")) {
				t.ambito = AmbitoLicitacion.SERVICIOS;
				t.min = 0;
				t.max = MAX_UTM;
				break BLOCK;
			}

		}

		return t;
	}

	private static int dameInteger(String s) {
		return Integer.parseInt(s.replaceAll("\\D", ""));
	}

	public static String deAccent(String str) {
		String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("");
	}
}