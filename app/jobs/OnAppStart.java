package jobs;

import java.io.IOException;

import play.Play;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

@OnApplicationStart
public class OnAppStart extends Job {

	public void doJob() {

		if (!Play.runingInTestMode())
			return;

		ProcessBuilder builder = new ProcessBuilder();
		builder.command("scripts/cargas-tablas.sh");

		try {
			Process process = builder.start();
			int exitCode = process.waitFor();
			assert exitCode == 0;
		} catch (IOException | InterruptedException e) {
			throw new RuntimeException(e.getMessage());
		}

	}
}
