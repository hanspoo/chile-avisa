package controllers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import models.Area;
import models.Licitacion;
import models.LicitacionDTO;
import models.Region;
import models.SubArea;
import models.dto.AreaDTO;
import play.Logger;
import play.mvc.Before;
import play.mvc.Controller;
import search.LicitacionesHomeService;
import search.BuscarPorSubAreaRegion;
import search.SearchService;
import utils.jpa.TotalesSubArea;

public class RestController extends Controller {

	@Before
	public static void action() {

		response.setHeader("Access-Control-Allow-Origin", "*");

	}

	public static void index() {
		render();
	}

	public static void licitaciones() {

		SearchService service = new LicitacionesHomeService();
		List<Licitacion> lics = service.execute();
		List<LicitacionDTO> collect = lics.stream().map(l -> l.dto()).collect(Collectors.toList());
		Set<LicitacionDTO> set = new HashSet<>(collect);
		renderJSON(set.stream().sorted().collect(Collectors.toList()));

	}

	public static void licitacionesHome() {

		SearchService service = new BuscarPorSubAreaRegion();
		List<Licitacion> lics = service.execute();
		List<LicitacionDTO> collect = lics.stream().map(l -> l.dto()).collect(Collectors.toList());
		Set<LicitacionDTO> set = new HashSet<>(collect);
		renderJSON(set.stream().sorted().collect(Collectors.toList()));

	}

	public static void filtros() {

		List<TotalesSubArea> subAreas = SubArea.totalesPorSubArea();

		Map<Area, Long> map = Area.totalesPorArea();
		List<AreaDTO> areas = Area.ordenadas().stream().map(a -> a.dto(map.get(a), subAreas))
				.collect(Collectors.toList());

		Map<String, List> finalMap = new HashMap<>();
		finalMap.put("areas", areas);
		finalMap.put("regiones", Region.totalesPorRegion());
		Logger.debug("Cargando areas");
		
		renderJSON(finalMap);

	}

}
