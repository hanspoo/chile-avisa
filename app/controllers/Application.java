package controllers;

import com.google.gson.Gson;

import loading.RecargarLicitaciones;
import play.mvc.Controller;

public class Application extends Controller {

	public static void index() {
		render();
	}

	public static void myview() {
		String nombre1 = "O'higgins";
		
		Gson g = new Gson();
		String j = g.toJson(nombre1);
		
		render(nombre1, j);
	}

	public static void recargarLicitaciones() {

		RecargarLicitaciones command = new RecargarLicitaciones();
		command.execute();
		flash.success("Listo");
		flash.keep();
		index();

	}

}